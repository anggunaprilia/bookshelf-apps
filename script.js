const bookshelfs = [];
const RENDER_EVENT = 'render-bookshelf';

const SAVED_EVENT = 'saved-bookshelf';
const STORAGE_KEY = 'BOOKSHELF_APPS';

const filter = document.getElementById('searchBookTitle');
const items = document.querySelectorAll('p')

document.addEventListener(RENDER_EVENT, function () {
    const uncompletedbookshelfsList = document.getElementById('bookshelfs');
    uncompletedbookshelfsList.innerHTML = '';

    const completedbookshelfsList = document.getElementById('completed-bookshelfs');
    completedbookshelfsList.innerHTML = '';

    for (const bookshelfsItem of bookshelfs) {
        const bookshelfsElement = makebookshelfs(bookshelfsItem);
        if (!bookshelfsItem.isCompleted)
        uncompletedbookshelfsList.append(bookshelfsElement);
        else
        completedbookshelfsList.append(bookshelfsElement);
    }
  });

document.addEventListener('DOMContentLoaded', function () {
    const submitForm = document.getElementById('form');
    submitForm.addEventListener('submit', function (event) {
      event.preventDefault();
      addbookshelfs();
      Swal.fire({
        title: 'Success',
        text: 'Berhasil Menambah Data Buku',
        icon: 'success',
        confirmButtonText: 'Oke'
      })
      submitForm.reset();
      return false;
    });

    if (isStorageExist()) {
        loadDataFromStorage();
      }
  });

  document.addEventListener(SAVED_EVENT, function () {
    console.log(localStorage.getItem(STORAGE_KEY));
  });

  function checkStatusBook() {
    const isCheckComplete = document.getElementById("isCompleted");
    if (isCheckComplete.checked) {
       return true;
    }
    return false;
 }

  function addbookshelfs() {
    const textbookshelfs = document.getElementById('title').value;
    const penulisbookshelfs = document.getElementById('penulis').value;
    const tahunbookshelfs = document.getElementById('tahun').value;
    const isCompleted = checkStatusBook();

    const generatedID = generateId();
    const bookshelfsObject = generatebookshelfsObject(generatedID, textbookshelfs, penulisbookshelfs,tahunbookshelfs, isCompleted);
    bookshelfs.push(bookshelfsObject);

    document.dispatchEvent(new Event(RENDER_EVENT));
    saveData();
  }

  function generateId() {
    return +new Date();
  }

  function generatebookshelfsObject(id, title, author,year, isCompleted) {
    return {
      id,
      title,
      author,
      year,
      isCompleted
    }
  }



  function makebookshelfs(bookshelfsObject) {
    const textTitle = document.createElement('h2');
    textTitle.innerText = bookshelfsObject.title;

    const penulisbookshelfs = document.createElement('p');
    penulisbookshelfs.innerText = "Penulis: "+bookshelfsObject.author;

    const tahunbookshelfs = document.createElement('p');
    tahunbookshelfs.innerText = "Tahun: " + bookshelfsObject.year;

    const textContainer = document.createElement('div');
    textContainer.classList.add('inner');
    textContainer.append(textTitle, penulisbookshelfs, tahunbookshelfs, );

    const container = document.createElement('div');
    container.classList.add('item', 'shadow');
    container.append(textContainer);
    container.setAttribute('id', `bookshelfs-${bookshelfsObject.id}`);



    if (bookshelfsObject.isCompleted) {
        const undoButton = document.createElement('button');
        undoButton.textContent = 'Belum Selesai Dibaca';
        undoButton.classList.add('undo-button');

        undoButton.addEventListener('click', function () {
          undoTaskFromCompleted(bookshelfsObject.id);
          Swal.fire({
            title: 'Success',
            text: 'Berhasil Memindahkan Buku',
            icon: 'success',
            confirmButtonText: 'Oke'
          })
        });

        const trashButton = document.createElement('button');
        trashButton.textContent = 'Hapus Buku';
        trashButton.classList.add('trash-button');

        trashButton.addEventListener('click', function () {
          removeTaskFromCompleted(bookshelfsObject.id);
          Swal.fire({
            title: 'Success',
            text: 'Berhasil Menghapus Buku',
            icon: 'success',
            confirmButtonText: 'Oke'
          })
        });

        container.append(undoButton, trashButton);
      } else {
        const checkButton = document.createElement('button');
        checkButton.textContent = 'Selesai Dibaca';
        checkButton.classList.add('check-button');

        checkButton.addEventListener('click', function () {
          addTaskToCompleted(bookshelfsObject.id);
          Swal.fire({
            title: 'Success',
            text: 'Berhasil Memindahkan Buku',
            icon: 'success',
            confirmButtonText: 'Oke'
          })
        });

        const trashButton = document.createElement('button');
        trashButton.textContent = 'Hapus Buku';
        trashButton.classList.add('trash-button');

        trashButton.addEventListener('click', function () {
          removeTaskFromCompleted(bookshelfsObject.id);
          Swal.fire({
            title: 'Success',
            text: 'Berhasil Menghapus Buku',
            icon: 'success',
            confirmButtonText: 'Oke'
          })
        });

        container.append(checkButton, trashButton);
      }

    return container;
  }

  function addTaskToCompleted (bookshelfsId) {
    const bookshelfsTarget = findbookshelfs(bookshelfsId);

    if (bookshelfsTarget == null) return;

    bookshelfsTarget.isCompleted = true;
    document.dispatchEvent(new Event(RENDER_EVENT));
    saveData();
  }

  function findbookshelfs(bookshelfsId) {
    for (const bookshelfsItem of bookshelfs) {
      if (bookshelfsItem.id === bookshelfsId) {
        return bookshelfsItem;
      }
    }
    return null;
  }

  function removeTaskFromCompleted(bookshelfsId) {
    const bookshelfsTarget = findbookshelfsIndex(bookshelfsId);

    if (bookshelfsTarget === -1) return;

    bookshelfs.splice(bookshelfsTarget, 1);
    document.dispatchEvent(new Event(RENDER_EVENT));
    saveData();
  }


  function undoTaskFromCompleted(bookshelfsId) {
    const bookshelfsTarget = findbookshelfs(bookshelfsId);

    if (bookshelfsTarget == null) return;

    bookshelfsTarget.isCompleted = false;
    document.dispatchEvent(new Event(RENDER_EVENT));
    saveData();
  }

  function findbookshelfsIndex(bookshelfsId) {
    for (const index in bookshelfs) {
      if (bookshelfs[index].id === bookshelfsId) {
        return index;
      }
    }

    return -1;
  }

  //save data ke storage
  function saveData() {
    if (isStorageExist()) {
      const parsed = JSON.stringify(bookshelfs);
      localStorage.setItem(STORAGE_KEY, parsed);
      document.dispatchEvent(new Event(SAVED_EVENT));
    }
  }

  function isStorageExist() /* boolean */ {
    if (typeof (Storage) === undefined) {
      alert('Browser kamu tidak mendukung local storage');
      return false;
    }
    return true;
  }

  function loadDataFromStorage() {
    const serializedData = localStorage.getItem(STORAGE_KEY);
    let data = JSON.parse(serializedData);

    if (data !== null) {
      for (const bookshelf of data) {
        bookshelfs.push(bookshelf);
      }
    }

    document.dispatchEvent(new Event(RENDER_EVENT));
  }

filter.addEventListener('input', (e) => filterData(e.target.value));

function filterData(search){
  items.forEach((item) => {
    if(item.innerText.toLowerCase().includes(search.toLowerCase())){
      item.classList.remove('d-none')
    }else{
      item.classList.add('d-none')
    }
  })
}

